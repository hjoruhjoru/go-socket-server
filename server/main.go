package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
	"os/exec"
)

func main() {

	// Create a TCP socket
	ln, err := net.Listen("tcp", ":8080")
	if err != nil {
		fmt.Println("Error creating socket:", err)
		return
	}
	defer ln.Close()

	clientCmds, err := excuteClients()
	if err != nil {
		return
	}

	clients := listenClientConn(ln, clientCmds)

	scanner := bufio.NewScanner(os.Stdin)
	for {
		fmt.Print("Type intergers and then click [ENTER]: ")
		scanner.Scan()
		message := scanner.Text()
		handleClient(clients, message)
		if message == "Q" {
			return
		}
	}

}

func handleClient(clients []net.Conn, message string) {
	for _, client := range clients {
		_, err := client.Write([]byte(message + "\n"))
		if err != nil {
			log.Println("Error sending data to client: ", err)
			break
		}
		if message == "Q" {
			continue
		}
		receive := make([]byte, 256)
		length, _ := client.Read(receive)
		fmt.Println(string(receive[:length]))
	}
}

func listenClientConn(ln net.Listener, clientCmds []*exec.Cmd) []net.Conn {
	var clients []net.Conn
	clientCount := 0
	for clientCount < len(clientCmds) {
		conn, err := ln.Accept()
		if err != nil {
			log.Println("Error accepting connection: ", err)
			continue
		}
		clients = append(clients, conn)
		clientCount++
	}
	return clients
}

func excuteClients() ([]*exec.Cmd, error) {
	clientCmds := []*exec.Cmd{
		exec.Command("go", "run", "client1/main.go"),
		exec.Command("go", "run", "client2/main.go"),
		exec.Command("go", "run", "client3/main.go"),
	}
	var err error
	for _, cmd := range clientCmds {
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		if err = cmd.Start(); err != nil {
			fmt.Println("Failed to start client process:", err)
		}
	}
	return clientCmds, err
}
