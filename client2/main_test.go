package main

import (
	"net"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestProcessReceiveData(t *testing.T) {
	ln, err := net.Listen("tcp", ":8000")
	defer ln.Close()

	conn, err := net.Dial("tcp", ln.Addr().String())
	if err != nil {
		t.Fatalf("Failed to connect to listener: %v", err)
	}
	defer conn.Close()
	processReceiveData("23 432 56", conn)

	clientConn, err := ln.Accept()
	if err != nil {
		return
	}
	receive := make([]byte, 256)
	length, _ := clientConn.Read(receive)
	assert.Equal(t, "Median is 56", string(receive[:length]))
}
