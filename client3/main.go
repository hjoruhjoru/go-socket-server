package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"strconv"
	"strings"
)

func main() {
	// Connect to server socket
	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		fmt.Println("Error connecting to server:", err)
		return
	}
	defer conn.Close()

	// Read data from server
	for {
		scanner := bufio.NewScanner(conn)
		for scanner.Scan() {
			message := scanner.Text()
			if message == "Q" {
				return
			}
			processReceiveData(message, conn)
		}
	}
}

func processReceiveData(message string, conn net.Conn) {
	numbersStr := strings.Fields(message)
	nums := make([]int, 0, len(numbersStr))
	for _, numStr := range numbersStr {
		num, err := strconv.Atoi(strings.TrimSpace(numStr))
		if err != nil {
			log.Printf("Invalid input: %v", err)
			continue
		}
		nums = append(nums, num)
	}
	mode := calculateMode(nums)
	conn.Write([]byte(("Mode is " + mode)))
}

func calculateMode(nums []int) string {
	if len(nums) == 0 {
		return "none"
	}
	counts := make(map[int]int)
	for _, val := range nums {
		counts[val]++
	}
	maxCount := 0
	mode := 0
	for num, count := range counts {
		if count > maxCount {
			maxCount = count
			mode = num
		}
	}
	return strconv.Itoa(mode)
}
