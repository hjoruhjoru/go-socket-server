package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"sort"
	"strconv"
	"strings"
)

func main() {
	// Connect to server socket
	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		fmt.Println("Error connecting to server:", err)
		return
	}
	defer conn.Close()

	// Read data from server
	for {
		scanner := bufio.NewScanner(conn)
		for scanner.Scan() {
			message := scanner.Text()
			if message == "Q" {
				return
			}
			processReceiveData(message, conn)
		}
	}
}

func processReceiveData(message string, conn net.Conn) {
	numbersStr := strings.Fields(message)
	nums := make([]int, 0, len(numbersStr))
	for _, numStr := range numbersStr {
		num, err := strconv.Atoi(strings.TrimSpace(numStr))
		if err != nil {
			log.Printf("Invalid input: %v", err)
			continue
		}
		nums = append(nums, num)
	}
	mean := calculateMedian(nums)
	conn.Write([]byte(("Median is " + mean)))
}

func calculateMedian(nums []int) string {
	if len(nums) == 0 {
		return "none"
	}
	sort.Ints(nums)
	length := len(nums)
	if length%2 == 0 {
		mid := length / 2
		return strconv.Itoa((nums[mid-1] + nums[mid]) / 2)
	} else {
		return strconv.Itoa(nums[length/2])
	}
}
