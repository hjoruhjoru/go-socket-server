package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"strconv"
	"strings"
)

func main() {
	// Connect to server socket
	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		fmt.Println("Error connecting to server:", err)
		return
	}
	defer conn.Close()

	// Read data from server
	for {
		scanner := bufio.NewScanner(conn)
		for scanner.Scan() {
			message := scanner.Text()
			if message == "Q" {
				return
			}
			processReceiveData(scanner.Text(), conn)

		}
	}
}

func processReceiveData(message string, conn net.Conn) {
	numbersStr := strings.Fields(message)
	nums := make([]int, 0, len(numbersStr))
	for _, numStr := range numbersStr {
		num, err := strconv.Atoi(strings.TrimSpace(numStr))
		if err != nil {
			log.Printf("Invalid input: %v", err)
			continue
		}
		nums = append(nums, num)
	}
	mean := calculateMean(nums)
	conn.Write([]byte(("Mean is " + mean)))
}

func calculateMean(nums []int) string {
	if len(nums) == 0 {
		return "none"
	}
	sum := 0
	for _, val := range nums {
		sum += val
	}
	mean := float64(sum) / float64(len(nums))
	return fmt.Sprintf("%f", mean)
}
